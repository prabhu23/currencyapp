package com.currencytocurrencyapp

import android.app.Application
import com.dynatrace.android.agent.Dynatrace
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CoinApplication: Application(){

    override fun onCreate() {
        super.onCreate()
    }
}