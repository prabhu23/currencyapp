package com.currencytocurrencyapp.data.remote


import com.currencytocurrencyapp.data.remote.dto.ConvertResponseDto
import com.currencytocurrencyapp.data.remote.dto.SymbolResponseDto
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface CurrentApi {

    @GET("symbols")
    suspend fun getSymbols(): SymbolResponseDto

    @GET("/convert?{to}&{from}&{amount}")
    suspend fun getConvertedCurrency(@Path("to") to: String,
                           @Path("from") from: String,
                           @Path("amount") amount: Int): ConvertResponseDto

//    @POST("/timeseries")
//    suspend fun getHistory(@Body loginres: LoginRequest): LoginResponse
}