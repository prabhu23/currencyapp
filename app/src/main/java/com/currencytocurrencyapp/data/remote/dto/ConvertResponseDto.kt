package com.currencytocurrencyapp.data.remote.dto


import com.currencytocurrencyapp.data.remote.model.Info
import com.currencytocurrencyapp.data.remote.model.Query
import com.currencytocurrencyapp.domain.model.ConvertedCurrency
import com.google.gson.annotations.SerializedName

data class ConvertResponseDto(
    @SerializedName("date")
    val date: String,
    @SerializedName("info")
    val info: Info,
    @SerializedName("query")
    val query: Query,
    @SerializedName("result")
    val result: Double,
    @SerializedName("success")
    val success: Boolean
)
fun ConvertResponseDto.toConvertedCurrency(): ConvertedCurrency {
    return ConvertedCurrency(from = query.from, to = query.to, amount = query.amount, convertedAmount = result.toString())
}