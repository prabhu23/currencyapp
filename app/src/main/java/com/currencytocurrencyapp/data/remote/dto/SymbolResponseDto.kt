package com.currencytocurrencyapp.data.remote.dto


import android.util.Log
import com.currencytocurrencyapp.domain.model.ConvertedCurrency
import com.currencytocurrencyapp.domain.model.Symbol
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

data class SymbolResponseDto(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("symbols")
    val symbols: SymbolsDto
)
fun SymbolResponseDto.toSymbols(): List<Symbol> {
    return symbols.toSymbols()
}