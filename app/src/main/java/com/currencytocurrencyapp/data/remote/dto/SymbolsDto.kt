package com.currencytocurrencyapp.data.remote.dto


import android.util.Log
import com.currencytocurrencyapp.data.remote.dto.SymbolResponseDto
import com.currencytocurrencyapp.domain.model.Symbol
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

data class SymbolsDto(
    @SerializedName("AED")
    val aED: String,
    @SerializedName("AFN")
    val aFN: String,
    @SerializedName("ALL")
    val aLL: String,
    @SerializedName("AMD")
    val aMD: String,
    @SerializedName("ANG")
    val aNG: String,
    @SerializedName("AOA")
    val aOA: String,
    @SerializedName("ARS")
    val aRS: String,
    @SerializedName("AUD")
    val aUD: String,
    @SerializedName("AWG")
    val aWG: String,
    @SerializedName("AZN")
    val aZN: String,
    @SerializedName("BAM")
    val bAM: String,
    @SerializedName("BBD")
    val bBD: String,
    @SerializedName("BDT")
    val bDT: String,
    @SerializedName("BGN")
    val bGN: String,
    @SerializedName("BHD")
    val bHD: String,
    @SerializedName("BIF")
    val bIF: String,
    @SerializedName("BMD")
    val bMD: String,
    @SerializedName("BND")
    val bND: String,
    @SerializedName("BOB")
    val bOB: String,
    @SerializedName("BRL")
    val bRL: String,
    @SerializedName("BSD")
    val bSD: String,
    @SerializedName("BTC")
    val bTC: String,
    @SerializedName("BTN")
    val bTN: String,
    @SerializedName("BWP")
    val bWP: String,
    @SerializedName("BYN")
    val bYN: String,
    @SerializedName("BYR")
    val bYR: String,
    @SerializedName("BZD")
    val bZD: String,
    @SerializedName("CAD")
    val cAD: String,
    @SerializedName("CDF")
    val cDF: String,
    @SerializedName("CHF")
    val cHF: String,
    @SerializedName("CLF")
    val cLF: String,
    @SerializedName("CLP")
    val cLP: String,
    @SerializedName("CNY")
    val cNY: String,
    @SerializedName("COP")
    val cOP: String,
    @SerializedName("CRC")
    val cRC: String,
    @SerializedName("CUC")
    val cUC: String,
    @SerializedName("CUP")
    val cUP: String,
    @SerializedName("CVE")
    val cVE: String,
    @SerializedName("CZK")
    val cZK: String,
    @SerializedName("DJF")
    val dJF: String,
    @SerializedName("DKK")
    val dKK: String,
    @SerializedName("DOP")
    val dOP: String,
    @SerializedName("DZD")
    val dZD: String,
    @SerializedName("EGP")
    val eGP: String,
    @SerializedName("ERN")
    val eRN: String,
    @SerializedName("ETB")
    val eTB: String,
    @SerializedName("EUR")
    val eUR: String,
    @SerializedName("FJD")
    val fJD: String,
    @SerializedName("FKP")
    val fKP: String,
    @SerializedName("GBP")
    val gBP: String,
    @SerializedName("GEL")
    val gEL: String,
    @SerializedName("GGP")
    val gGP: String,
    @SerializedName("GHS")
    val gHS: String,
    @SerializedName("GIP")
    val gIP: String,
    @SerializedName("GMD")
    val gMD: String,
    @SerializedName("GNF")
    val gNF: String,
    @SerializedName("GTQ")
    val gTQ: String,
    @SerializedName("GYD")
    val gYD: String,
    @SerializedName("HKD")
    val hKD: String,
    @SerializedName("HNL")
    val hNL: String,
    @SerializedName("HRK")
    val hRK: String,
    @SerializedName("HTG")
    val hTG: String,
    @SerializedName("HUF")
    val hUF: String,
    @SerializedName("IDR")
    val iDR: String,
    @SerializedName("ILS")
    val iLS: String,
    @SerializedName("IMP")
    val iMP: String,
    @SerializedName("INR")
    val iNR: String,
    @SerializedName("IQD")
    val iQD: String,
    @SerializedName("IRR")
    val iRR: String,
    @SerializedName("ISK")
    val iSK: String,
    @SerializedName("JEP")
    val jEP: String,
    @SerializedName("JMD")
    val jMD: String,
    @SerializedName("JOD")
    val jOD: String,
    @SerializedName("JPY")
    val jPY: String,
    @SerializedName("KES")
    val kES: String,
    @SerializedName("KGS")
    val kGS: String,
    @SerializedName("KHR")
    val kHR: String,
    @SerializedName("KMF")
    val kMF: String,
    @SerializedName("KPW")
    val kPW: String,
    @SerializedName("KRW")
    val kRW: String,
    @SerializedName("KWD")
    val kWD: String,
    @SerializedName("KYD")
    val kYD: String,
    @SerializedName("KZT")
    val kZT: String,
    @SerializedName("LAK")
    val lAK: String,
    @SerializedName("LBP")
    val lBP: String,
    @SerializedName("LKR")
    val lKR: String,
    @SerializedName("LRD")
    val lRD: String,
    @SerializedName("LSL")
    val lSL: String,
    @SerializedName("LTL")
    val lTL: String,
    @SerializedName("LVL")
    val lVL: String,
    @SerializedName("LYD")
    val lYD: String,
    @SerializedName("MAD")
    val mAD: String,
    @SerializedName("MDL")
    val mDL: String,
    @SerializedName("MGA")
    val mGA: String,
    @SerializedName("MKD")
    val mKD: String,
    @SerializedName("MMK")
    val mMK: String,
    @SerializedName("MNT")
    val mNT: String,
    @SerializedName("MOP")
    val mOP: String,
    @SerializedName("MRO")
    val mRO: String,
    @SerializedName("MUR")
    val mUR: String,
    @SerializedName("MVR")
    val mVR: String,
    @SerializedName("MWK")
    val mWK: String,
    @SerializedName("MXN")
    val mXN: String,
    @SerializedName("MYR")
    val mYR: String,
    @SerializedName("MZN")
    val mZN: String,
    @SerializedName("NAD")
    val nAD: String,
    @SerializedName("NGN")
    val nGN: String,
    @SerializedName("NIO")
    val nIO: String,
    @SerializedName("NOK")
    val nOK: String,
    @SerializedName("NPR")
    val nPR: String,
    @SerializedName("NZD")
    val nZD: String,
    @SerializedName("OMR")
    val oMR: String,
    @SerializedName("PAB")
    val pAB: String,
    @SerializedName("PEN")
    val pEN: String,
    @SerializedName("PGK")
    val pGK: String,
    @SerializedName("PHP")
    val pHP: String,
    @SerializedName("PKR")
    val pKR: String,
    @SerializedName("PLN")
    val pLN: String,
    @SerializedName("PYG")
    val pYG: String,
    @SerializedName("QAR")
    val qAR: String,
    @SerializedName("RON")
    val rON: String,
    @SerializedName("RSD")
    val rSD: String,
    @SerializedName("RUB")
    val rUB: String,
    @SerializedName("RWF")
    val rWF: String,
    @SerializedName("SAR")
    val sAR: String,
    @SerializedName("SBD")
    val sBD: String,
    @SerializedName("SCR")
    val sCR: String,
    @SerializedName("SDG")
    val sDG: String,
    @SerializedName("SEK")
    val sEK: String,
    @SerializedName("SGD")
    val sGD: String,
    @SerializedName("SHP")
    val sHP: String,
    @SerializedName("SLE")
    val sLE: String,
    @SerializedName("SLL")
    val sLL: String,
    @SerializedName("SOS")
    val sOS: String,
    @SerializedName("SRD")
    val sRD: String,
    @SerializedName("STD")
    val sTD: String,
    @SerializedName("SVC")
    val sVC: String,
    @SerializedName("SYP")
    val sYP: String,
    @SerializedName("SZL")
    val sZL: String,
    @SerializedName("THB")
    val tHB: String,
    @SerializedName("TJS")
    val tJS: String,
    @SerializedName("TMT")
    val tMT: String,
    @SerializedName("TND")
    val tND: String,
    @SerializedName("TOP")
    val tOP: String,
    @SerializedName("TRY")
    val tRY: String,
    @SerializedName("TTD")
    val tTD: String,
    @SerializedName("TWD")
    val tWD: String,
    @SerializedName("TZS")
    val tZS: String,
    @SerializedName("UAH")
    val uAH: String,
    @SerializedName("UGX")
    val uGX: String,
    @SerializedName("USD")
    val uSD: String,
    @SerializedName("UYU")
    val uYU: String,
    @SerializedName("UZS")
    val uZS: String,
    @SerializedName("VEF")
    val vEF: String,
    @SerializedName("VND")
    val vND: String,
    @SerializedName("VUV")
    val vUV: String,
    @SerializedName("WST")
    val wST: String,
    @SerializedName("XAF")
    val xAF: String,
    @SerializedName("XAG")
    val xAG: String,
    @SerializedName("XAU")
    val xAU: String,
    @SerializedName("XCD")
    val xCD: String,
    @SerializedName("XDR")
    val xDR: String,
    @SerializedName("XOF")
    val xOF: String,
    @SerializedName("XPF")
    val xPF: String,
    @SerializedName("YER")
    val yER: String,
    @SerializedName("ZAR")
    val zAR: String,
    @SerializedName("ZMK")
    val zMK: String,
    @SerializedName("ZMW")
    val zMW: String,
    @SerializedName("ZWL")
    val zWL: String
)
fun SymbolsDto.toSymbols(): List<Symbol> {
    val symbolList: MutableList<Symbol> = mutableListOf<Symbol>()

    symbolList.add(Symbol(key= "AED", value = aED ))
    symbolList.add(Symbol(key= "AFN", value = aFN ))
    symbolList.add(Symbol(key= "ALL", value = aLL ))
    symbolList.add(Symbol(key= "AMD", value = aMD ))
    symbolList.add(Symbol(key= "ANG", value = aNG ))
    symbolList.add(Symbol(key= "AOA", value = aOA ))
    symbolList.add(Symbol(key= "ARS", value = aRS ))
    symbolList.add(Symbol(key= "AUD", value = aUD ))
    symbolList.add(Symbol(key= "AWG", value = aWG ))
    symbolList.add(Symbol(key= "AZN", value = aZN ))
    symbolList.add(Symbol(key= "BAM", value = bAM ))
    symbolList.add(Symbol(key= "BBD", value = bBD ))
    symbolList.add(Symbol(key= "BDT", value = bDT ))
    symbolList.add(Symbol(key= "BGN", value = bGN ))
    symbolList.add(Symbol(key= "BHD", value = bHD ))
    symbolList.add(Symbol(key= "BIF", value = bIF ))
    symbolList.add(Symbol(key= "BMD", value = bMD ))
    symbolList.add(Symbol(key= "BND", value = bND ))
    symbolList.add(Symbol(key= "BOB", value = bOB ))
    symbolList.add(Symbol(key= "BRL", value = bRL ))
    symbolList.add(Symbol(key= "BSD", value = bSD ))
    symbolList.add(Symbol(key= "BTC", value = bTC ))
    symbolList.add(Symbol(key= "BTN", value = bTN ))
    symbolList.add(Symbol(key= "BWP", value = bWP ))
    symbolList.add(Symbol(key= "BYN", value = bYN ))
    symbolList.add(Symbol(key= "BYR", value = bYR ))
    symbolList.add(Symbol(key= "BZD", value = bZD ))
    symbolList.add(Symbol(key= "CAD", value = cAD ))
    symbolList.add(Symbol(key= "CDF", value = cDF ))
    symbolList.add(Symbol(key= "CHF", value = cHF ))
    symbolList.add(Symbol(key= "CLF", value = cLF ))
    symbolList.add(Symbol(key= "CLP", value = cLP ))
    symbolList.add(Symbol(key= "CNY", value = cNY ))
    symbolList.add(Symbol(key= "COP", value = cOP ))
    symbolList.add(Symbol(key= "CRC", value = cRC ))
    symbolList.add(Symbol(key= "CUC", value = cUC ))
    symbolList.add(Symbol(key= "CUP", value = cUP ))
    symbolList.add(Symbol(key= "CVE", value = cVE ))
    symbolList.add(Symbol(key= "CZK", value = cZK ))
    symbolList.add(Symbol(key= "CZK", value = cZK ))
    symbolList.add(Symbol(key= "DKK", value = dKK ))
    symbolList.add(Symbol(key= "DOP", value = dOP ))
    symbolList.add(Symbol(key= "DZD", value = dZD ))
    symbolList.add(Symbol(key= "EGP", value = eGP ))
    symbolList.add(Symbol(key= "ERN", value = eRN ))
    symbolList.add(Symbol(key= "ETB", value = eTB ))
    symbolList.add(Symbol(key= "EUR", value = eUR ))
    symbolList.add(Symbol(key= "FJD", value = fJD ))
    symbolList.add(Symbol(key= "FKP", value = fKP ))
    symbolList.add(Symbol(key= "GBP", value = gBP ))
    symbolList.add(Symbol(key= "GEL", value = gEL ))
    symbolList.add(Symbol(key= "GGP", value = gGP ))
    symbolList.add(Symbol(key= "GHS", value = gHS ))
    symbolList.add(Symbol(key= "GIP", value = gIP ))
    symbolList.add(Symbol(key= "GMD", value = gMD ))
    symbolList.add(Symbol(key= "GNF", value = gNF ))
    symbolList.add(Symbol(key= "GTQ", value = gTQ ))
    symbolList.add(Symbol(key= "GYD", value = gYD ))
    symbolList.add(Symbol(key= "HKD", value = hKD ))
    symbolList.add(Symbol(key= "HNL", value = hNL ))
    symbolList.add(Symbol(key= "HRK", value = hRK ))
    symbolList.add(Symbol(key= "HTG", value = hTG ))
    symbolList.add(Symbol(key= "HUF", value = hUF ))
    symbolList.add(Symbol(key= "IDR", value = iDR ))
    symbolList.add(Symbol(key= "ILS", value = iLS ))
    symbolList.add(Symbol(key= "IMP", value = iMP ))
    symbolList.add(Symbol(key= "INR", value = iNR ))
    symbolList.add(Symbol(key= "IQD", value = iQD ))
    symbolList.add(Symbol(key= "IRR", value = iRR ))
    symbolList.add(Symbol(key= "ISK", value = iSK ))
    symbolList.add(Symbol(key= "JEP", value = jEP ))
    symbolList.add(Symbol(key= "JMD", value = jMD ))
    symbolList.add(Symbol(key= "JOD", value = jOD ))
    symbolList.add(Symbol(key= "JPY", value = jPY ))
    symbolList.add(Symbol(key= "KES", value = kES ))
    symbolList.add(Symbol(key= "KGS", value = kGS ))
    symbolList.add(Symbol(key= "KHR", value = kHR ))
    symbolList.add(Symbol(key= "KMF", value = kMF ))
    symbolList.add(Symbol(key= "KPW", value = kPW ))
    symbolList.add(Symbol(key= "KRW", value = kRW ))
    symbolList.add(Symbol(key= "KWD", value = kWD ))
    symbolList.add(Symbol(key= "KYD", value = kYD ))
    symbolList.add(Symbol(key= "KZT", value = kZT ))
    symbolList.add(Symbol(key= "LAK", value = lAK ))
    symbolList.add(Symbol(key= "LBP", value = lBP ))
    symbolList.add(Symbol(key= "LKR", value = lKR ))
    symbolList.add(Symbol(key= "LRD", value = lRD ))
    symbolList.add(Symbol(key= "LSL", value = lSL ))
    symbolList.add(Symbol(key= "LTL", value = lTL ))
    symbolList.add(Symbol(key= "LVL", value = lVL ))
    symbolList.add(Symbol(key= "LYD", value = lYD ))
    symbolList.add(Symbol(key= "MAD", value = mAD ))
    symbolList.add(Symbol(key= "MGA", value = mGA ))
    symbolList.add(Symbol(key= "MKD", value = mKD ))
    symbolList.add(Symbol(key= "MMK", value = mMK ))
    symbolList.add(Symbol(key= "MNT", value = mNT ))
    symbolList.add(Symbol(key= "MOP", value = mOP ))
    symbolList.add(Symbol(key= "MRO", value = mRO ))
    symbolList.add(Symbol(key= "MUR", value = mUR ))
    symbolList.add(Symbol(key= "MVR", value = mVR ))
    symbolList.add(Symbol(key= "MWK", value = mWK ))
    symbolList.add(Symbol(key= "MXN", value = mXN ))
    symbolList.add(Symbol(key= "MYR", value = mYR ))
    symbolList.add(Symbol(key= "MZN", value = mZN ))
    symbolList.add(Symbol(key= "NAD", value = nAD ))
    symbolList.add(Symbol(key= "NGN", value = nGN ))
    symbolList.add(Symbol(key= "NIO", value = nIO ))
    symbolList.add(Symbol(key= "NOK", value = nOK ))
    symbolList.add(Symbol(key= "NPR", value = nPR ))
    symbolList.add(Symbol(key= "NZD", value = nZD ))
    symbolList.add(Symbol(key= "OMR", value = oMR ))
    symbolList.add(Symbol(key= "PAB", value = pAB ))
    symbolList.add(Symbol(key= "PEN", value = pEN ))
    symbolList.add(Symbol(key= "PGK", value = pGK ))
    symbolList.add(Symbol(key= "PHP", value = pHP ))
    symbolList.add(Symbol(key= "PKR", value = pKR ))
    symbolList.add(Symbol(key= "PLN", value = pLN ))
    symbolList.add(Symbol(key= "PYG", value = pYG ))
    symbolList.add(Symbol(key= "QAR", value = qAR ))
    symbolList.add(Symbol(key= "RON", value = rON ))
    symbolList.add(Symbol(key= "RSD", value = rSD ))
    symbolList.add(Symbol(key= "RUB", value = rUB ))
    symbolList.add(Symbol(key= "RWF", value = rWF ))
    symbolList.add(Symbol(key= "SAR", value = sAR ))
    symbolList.add(Symbol(key= "SBD", value = sBD ))
    symbolList.add(Symbol(key= "SCR", value = sCR ))
    symbolList.add(Symbol(key= "SDG", value = sDG ))
    symbolList.add(Symbol(key= "SEK", value = sEK ))
    symbolList.add(Symbol(key= "SGD", value = sGD ))
    symbolList.add(Symbol(key= "SHP", value = sHP ))
    symbolList.add(Symbol(key= "SLE", value = sLE ))
    symbolList.add(Symbol(key= "SLL", value = sLL ))
    symbolList.add(Symbol(key= "SOS", value = sOS ))
    symbolList.add(Symbol(key= "SRD", value = sRD ))
    symbolList.add(Symbol(key= "STD", value = sTD ))
    symbolList.add(Symbol(key= "SVC", value = sVC ))
    symbolList.add(Symbol(key= "SYP", value = sYP ))
    symbolList.add(Symbol(key= "SZL", value = sZL ))
    symbolList.add(Symbol(key= "THB", value = tHB ))
    symbolList.add(Symbol(key= "TJS", value = tJS ))
    symbolList.add(Symbol(key= "TMT", value = tMT ))
    symbolList.add(Symbol(key= "TND", value = tND ))
    symbolList.add(Symbol(key= "TOP", value = tOP ))
    symbolList.add(Symbol(key= "TRY", value = tRY ))
    symbolList.add(Symbol(key= "TTD", value = tTD ))
    symbolList.add(Symbol(key= "TWD", value = tWD ))
    symbolList.add(Symbol(key= "TZS", value = tZS ))
    symbolList.add(Symbol(key= "UAH", value = uAH ))
    symbolList.add(Symbol(key= "UGX", value = uGX ))
    symbolList.add(Symbol(key= "USD", value = uSD ))
    symbolList.add(Symbol(key= "UYU", value = uYU ))
    symbolList.add(Symbol(key= "UZS", value = uZS ))
    symbolList.add(Symbol(key= "VEF", value = vEF ))
    symbolList.add(Symbol(key= "VND", value = vND ))
    symbolList.add(Symbol(key= "VUV", value = vUV ))
    symbolList.add(Symbol(key= "WST", value = wST ))
    symbolList.add(Symbol(key= "XAF", value = xAF ))
    symbolList.add(Symbol(key= "XAG", value = xAG ))
    symbolList.add(Symbol(key= "XAU", value = xAU ))
    symbolList.add(Symbol(key= "XCD", value = xCD ))
    symbolList.add(Symbol(key= "XDR", value = xDR ))
    symbolList.add(Symbol(key= "XDR", value = xDR ))
    symbolList.add(Symbol(key= "XPF", value = xPF ))
    symbolList.add(Symbol(key= "YER", value = yER ))
    symbolList.add(Symbol(key= "ZAR", value = zAR ))
    symbolList.add(Symbol(key= "ZWL", value = zWL ))
    symbolList.add(Symbol(key= "ZMW", value = zMW ))
    symbolList.add(Symbol(key= "ZMK", value = zMK ))
    return symbolList
}