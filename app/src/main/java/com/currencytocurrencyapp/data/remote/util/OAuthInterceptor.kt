package com.currencytocurrencyapp.data.remote.util

import okhttp3.Interceptor

class OAuthInterceptor(private val tokenType: String, private val acceessToken: String): Interceptor {

    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        var request = chain.request()
        request = request.newBuilder().header("apikey", "$tokenType $acceessToken").build()

        return chain.proceed(request)
    }
}