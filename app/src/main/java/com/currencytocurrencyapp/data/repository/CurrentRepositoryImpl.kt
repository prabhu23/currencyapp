package com.currencytocurrencyapp.data.repository

import android.util.Log
import com.currencytocurrencyapp.data.remote.CurrentApi
import com.currencytocurrencyapp.data.remote.dto.toConvertedCurrency
import com.currencytocurrencyapp.data.remote.dto.toSymbols
import com.currencytocurrencyapp.domain.model.ConvertedCurrency
import com.currencytocurrencyapp.domain.model.Symbol
import com.currencytocurrencyapp.domain.repository.CurrencyRepository
import com.google.gson.Gson
import javax.inject.Inject


class CurrentRepositoryImpl @Inject constructor(
    private val api: CurrentApi): CurrencyRepository{

    override suspend fun getSymbols(): List<Symbol> {
       return  api.getSymbols().toSymbols()
    }

    override suspend fun getConvertedValue(from: String, to: String, amount: Int): ConvertedCurrency {
       return api.getConvertedCurrency(from, to, amount).toConvertedCurrency()
    }




}