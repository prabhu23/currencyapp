package com.currencytocurrencyapp.di

import com.currencytocurrencyapp.common.Constands
import com.currencytocurrencyapp.data.remote.CurrentApi
import com.currencytocurrencyapp.data.remote.util.OAuthInterceptor
import com.currencytocurrencyapp.data.repository.CurrentRepositoryImpl
import com.currencytocurrencyapp.domain.repository.CurrencyRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModel {
    val client =  OkHttpClient.Builder()
        .addInterceptor(OAuthInterceptor("", "PGHzyGuxMrrx6044nocWvUfOayPrCpmu"))
        .build()
    @Singleton
    @Provides
    fun provideCyptoApi(): CurrentApi{
        return Retrofit.Builder()
            .baseUrl(Constands.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(CurrentApi::class.java)
    }


    @Singleton
    @Provides
    fun provideCoinRepository(api: CurrentApi): CurrencyRepository{
        return CurrentRepositoryImpl(api)
    }
}