package com.currencytocurrencyapp.domain.model

data class ConvertedCurrency(val from: String, val to: String, val amount: Int, val convertedAmount: String)
