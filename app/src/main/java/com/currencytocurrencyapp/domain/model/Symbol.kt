package com.currencytocurrencyapp.domain.model

data class Symbol(val key: String, val value: String )
