package com.currencytocurrencyapp.domain.repository

import com.currencytocurrencyapp.domain.model.ConvertedCurrency
import com.currencytocurrencyapp.domain.model.Symbol

interface CurrencyRepository {
    open suspend fun getSymbols(): List<Symbol>

    open suspend fun getConvertedValue(from: String, to: String, amount: Int): ConvertedCurrency

}