package com.currencytocurrencyapp.domain.usecase.get_symbols

import android.util.Log
import com.currencytocurrencyapp.common.Resource
import com.currencytocurrencyapp.domain.model.Symbol
import com.currencytocurrencyapp.domain.repository.CurrencyRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class GetSymbolsUseCase @Inject constructor(
    private val repository: CurrencyRepository
) {
    operator fun invoke(): Flow<Resource<List<Symbol>>> = flow{
        try {
            emit(Resource.Loading<List<Symbol>>())
            val symbols = repository.getSymbols()

            emit(Resource.Success<List<Symbol>>(symbols))
        }catch (e: HttpException){
            emit(Resource.Error<List<Symbol>>(e.localizedMessage ?: "An unexpected error occured"))
        }catch (e: IOException){
            emit(Resource.Error<List<Symbol>>("Couldn't reach server. Check your internet connection."))
        }
    }

}