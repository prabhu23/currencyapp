package com.currencytocurrencyapp.presentation

sealed class Screen(val route: String){
    object CurrentConverter: Screen("current_converter");
}
