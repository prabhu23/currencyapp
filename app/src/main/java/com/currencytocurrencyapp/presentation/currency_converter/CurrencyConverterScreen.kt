package com.currencytocurrencyapp.presentation.currency_converter

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.currencytocurrencyapp.presentation.currency_converter.components.CurrencyConverterSymbolItems
import com.currencytocurrencyapp.presentation.currency_converter.components.dropDownMenu


@Composable
fun CurrencyConverterScreen(
    navController: NavController,
    viewModel: CurrencyConverterViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    dropDownMenu()
    Box(modifier = Modifier
        .padding(10.dp)
        .fillMaxSize()){
        LazyColumn(Modifier.fillMaxSize()){
            items(state.currentConverterSymbol){ symbol->
                CurrencyConverterSymbolItems(symbol)
            }
        }
        if(state.error.isNotBlank()){
            Text(
                text = state.error,
                color = MaterialTheme.colors.error,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                    .align(Alignment.Center)
            )
        }
        if(state.currentConverterLoading){
            CircularProgressIndicator(Modifier.align(Alignment.Center))
        }
    }
}
