package com.currencytocurrencyapp.presentation.currency_converter

import com.currencytocurrencyapp.domain.model.Symbol

data class CurrencyConverterState(
    val currentConverterLoading: Boolean = false,
    val currentConverterSymbol: List<Symbol> = emptyList(),
    val error: String = ""
)