package com.currencytocurrencyapp.presentation.currency_converter

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.currencytocurrencyapp.common.Resource
import com.currencytocurrencyapp.domain.model.Symbol
import com.currencytocurrencyapp.domain.usecase.get_symbols.GetSymbolsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject
@HiltViewModel
class CurrencyConverterViewModel @Inject constructor(
 private val getSymbolsUseCase: GetSymbolsUseCase
): ViewModel() {
    private val _state = mutableStateOf(CurrencyConverterState())
    val state: MutableState<CurrencyConverterState> = _state
    init {
        getSymbols()
    }

    private fun getSymbols(){
        getSymbolsUseCase().onEach { result ->
            when(result){
                is Resource.Success ->{

                    _state.value = CurrencyConverterState(currentConverterSymbol = (result.data?: emptyList<Symbol>()) as List<Symbol>)
                }
                is Resource.Error ->{
                    _state.value = CurrencyConverterState(error = result.message ?: "An unexpected error occured")
                }
                is Resource.Loading ->{
                    _state.value = CurrencyConverterState(currentConverterLoading = true)
                }
            }
        }.launchIn(viewModelScope)
    }
}
