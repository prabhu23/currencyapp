package com.currencytocurrencyapp.presentation.currency_converter.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.currencytocurrencyapp.domain.model.Symbol


@Composable
fun CurrencyConverterSymbolItems(symbol: Symbol) {
    Row(
    Modifier
    .fillMaxWidth()
    .clickable {   },horizontalArrangement = Arrangement.SpaceBetween) {
        Text(text = "${symbol.key } (${symbol.value})",
            style = MaterialTheme.typography.body1,
            overflow = TextOverflow.Ellipsis)

        Spacer(Modifier.size(5.dp))
    }

}